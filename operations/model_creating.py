import pandas as pd
from datasets import load_dataset
from sentence_transformers.losses import CosineSimilarityLoss

from setfit import SetFitModel, SetFitTrainer

def model_process(path, data_path, output_path, report_path):
    try:
        dataset = load_dataset('csv', data_files={
            'train': [path+'train.csv'],
            'eval': [path+'eval.csv']}
        )

        # Load a SetFit model from Hub
        model = SetFitModel.from_pretrained(
            "sentence-transformers/paraphrase-multilingual-MiniLM-L12-v2",
            cache_dir="./models/"
        )

        # Create trainer
        trainer = SetFitTrainer(
            model=model,
            train_dataset=dataset['train'],
            eval_dataset=dataset['eval'],
            loss_class=CosineSimilarityLoss,
            metric="accuracy",
            learning_rate=0.0001,
            num_epochs=1,
            batch_size=32,
            num_iterations=50,
            seed=40,
            column_mapping={"text": "text", "label": "label"}
        )

        # Train and evaluate
        trainer.train()
        metrics = trainer.evaluate()
        print(metrics)

        df = pd.DataFrame(metrics, columns=["parameter", "value"])
        df.to_csv(report_path+"model_output.csv", index=False)
        # save
        trainer.model._save_pretrained(save_directory=output_path)

        return "complate"

    except Exception as e:
        print(e)
