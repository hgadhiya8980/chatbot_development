from setfit import SetFitModel

def prediction_model(text, sentiment_dict, output_path):
    try:
        model = SetFitModel.from_pretrained(output_path, local_files_only=True)
        inverse_dict = {value: key for (key, value) in sentiment_dict.items()}
        li = [text]
        preds = model(li)
        h2 = model.predict_proba([text])

        for i in range(len(li)):
            return inverse_dict[int(preds[i])], int(preds[i]), round(float(h2[0][int(preds[i])]*100), 2), h2

    except Exception as e:
        print(e)
