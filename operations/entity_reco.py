import os
os.environ["OPENAI_API_KEY"] = "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"

from langchain.llms import OpenAI
from langchain.prompts import PromptTemplate

llm = OpenAI(temperature=0.9)

prompt = PromptTemplate(
    input_variables = ["given_content", "question"],
    template = "Please provide the appropriate response based on the given content. If the question is not present in the content, please provide the response, Please contact us for further assistance. Please respond as if you were a human chatbot, without mentioning that you are an AI. given content={given_content} question={question}"
)

given_content = "Rylee IT Solutions is a cutting-edge technology company that offers a comprehensive range of services tailored to meet the diverse needs of modern businesses. Our services encompass a wide spectrum of IT solutions, including cloud computing, cybersecurity, software development, network infrastructure, and IT consulting. With a team of highly skilled and certified professionals, we deliver customized solutions that empower organizations to optimize their operations, enhance productivity, and achieve strategic goals. Our client base spans across various industries, including finance, healthcare, manufacturing, education, and government sectors. We have established strong partnerships with both small businesses and Fortune 500 companies, earning a reputation for reliability, innovation, and exceptional customer support. The content associated with our company includes a rich knowledge base of articles, whitepapers, and case studies, covering topics ranging from emerging technologies to best practices in IT management. Additionally, we regularly publish insightful blogs and host webinars to keep our clients and the broader community updated on the latest trends and advancements in the IT industry. In terms of products, we offer a suite of proprietary software solutions designed to streamline operations and improve efficiency. These include project management tools, CRM systems, and cybersecurity applications, all of which are built with a focus on scalability, security, and user-friendliness. Through our unwavering commitment to innovation and client satisfaction, Rylee IT Solutions stands as a trusted partner in the ever-evolving landscape of information technology. if you have any query please reach out 9316727742."
question = "what service provided by your company?"

formating = prompt.format(given_content = given_content, question=question)

output_name = llm(formating)
print(output_name)


