import datetime
import json

from flask import (flash, Flask, redirect, render_template, request,
                   session, url_for)
import os
from pymongo import MongoClient
import pandas as pd
from datasets import load_dataset
from sentence_transformers.losses import CosineSimilarityLoss
from setfit import SetFitModel, SetFitTrainer
import random
from langchain.llms import OpenAI
from langchain.prompts import PromptTemplate

# create a flask server 
app = Flask(__name__)
UPLOAD_FOLDER = 'static/uploads/'

# create a dynamic variable for my project purpose
app.config["SECRET_KEY"] = "sdfsf65416534sdfsdf4653"
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
os.environ["OPENAI_API_KEY"] = "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"
app.config["SPACY_DICT"] = {}
secure_type = "http"
app.config["COUNT"] = 1
llm = OpenAI(temperature=0.9)


ALLOWED_EXTENSIONS = ['csv', 'xlsx']

# connect my mongodb cloud
client = MongoClient("mongodb+srv://jaykakadiyawork:FB30fRUM8DV0U1xk@cluster0.wwspsfj.mongodb.net/?retryWrites=true&w=majority")

# connect with my database
db = client["chatbot_developement"]

# checking file extension like i want allow only csv and excel file
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# data add in my mongo database
def register_data(coll_name, new_dict):
    try:
        coll = db[coll_name]
        coll.insert_one(new_dict)

        return "add_data"

    except Exception as e:
        print(e)

# get all data from my table from databse
def find_all_cust_details_coll(coll_name):
    try:
        coll = db[coll_name]
        res = coll.find({})
        return res

    except Exception as e:
        print(e)

# get only specific data from my database
def find_all_specific_user(coll_name, di):
    try:
        coll = db[coll_name]
        res = coll.find(di)
        return res

    except Exception as e:
        print(e)

# delete data from database for our condition
def delete_data(coll_name, di):
    try:
        coll = db[coll_name]
        res = coll.delete_one(di)
        return res

    except Exception as e:
        print(e)

# update data from database
def update_mongo_data(coll_name, prev_data, update_data):
    try:
        coll = db[coll_name]
        coll.update_one(prev_data, {"$set": update_data})
        return "updated"

    except Exception as e:
        print(e)

# getting all file from folder
def get_folder_name():
    try:
        output_path = os.path.abspath("output")
        folder_names = [name for name in os.listdir(output_path) if os.path.isdir(os.path.join(output_path, name))]
        return folder_names

    except Exception as e:
        print(e)

# prediction function that can predict intent from my setfit model
def prediction_model(text, sentiment_dict, output_path):
    try:
        model = SetFitModel.from_pretrained(output_path, local_files_only=True)
        inverse_dict = {value: key for (key, value) in sentiment_dict.items()}
        li = [text]
        preds = model(li)
        h2 = model.predict_proba([text])

        for i in range(len(li)):
            return inverse_dict[int(preds[i])], int(preds[i]), round(float(h2[0][int(preds[i])]*100), 2), h2

    except Exception as e:
        print(e)

# model building using training and validation dataset. 
def model_process(path, data_path, output_path, report_path):
    try:
        dataset = load_dataset('csv', data_files={
            'train': [path+'train.csv'],
            'eval': [path+'eval.csv']}
        )

        # Load a SetFit model from Hub
        model = SetFitModel.from_pretrained(
            "sentence-transformers/paraphrase-multilingual-MiniLM-L12-v2",
            cache_dir="./models/"
        )

        # Create trainer
        trainer = SetFitTrainer(
            model=model,
            train_dataset=dataset['train'],
            eval_dataset=dataset['eval'],
            loss_class=CosineSimilarityLoss,
            metric="accuracy",
            learning_rate=0.0001,
            num_epochs=1,
            batch_size=32,
            num_iterations=50,
            seed=40,
            column_mapping={"text": "text", "label": "label"}
        )

        # Train and evaluate
        trainer.train()
        metrics = trainer.evaluate()
        print(metrics)

        df = pd.DataFrame(metrics, columns=["parameter", "value"])
        df.to_csv(report_path+"model_output.csv", index=False)
        # save
        trainer.model._save_pretrained(save_directory=output_path)

        return "complate"

    except Exception as e:
        print(e)

def get_data_response(cp_name, intent_name):
    try:
        res_di = {"cp_name": cp_name}
        all_res_data = find_all_specific_user(coll_name="response_data", di=res_di)
        res = [dat["response"] for dat in all_res_data if dat["intent_name"]==intent_name]
        return random.choice(res[0])

    except Exception as e:
        print(e)

def get_res_by_chatgpt(text, all_content):
    try:
        # api_key = "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"
        # all_data = f"Please provide a human-like answer based on the given content. Keep the answer concise, like a response from a chatbot. content={all_content} and question={text}"
        # url = "https://api.openai.com/v1/chat/completions"
        # payload = {
        #     "model": "gpt-3.5-turbo",
        #     "messages": [{"role": "user", "content": all_data}]
        # }
        #
        # headers = {
        #     "Content-Type": "application/json",
        #     "Authorization": f"Bearzer {api_key}"
        # }
        #
        # response = requests.post(url, headers=headers, json=payload, stream=False)
        # response1 = json.loads(response.content)
        # output_name = response1["choices"][0]["message"]["content"]
        prompt = PromptTemplate(
            input_variables=["all_content", "userMessage"],
            template="Please provide a human-like answer based on the given content. Please note when question answer not provide in given content then only provide simple contact us response, without mentioning that you are an AI or an agent. Keep the answer concise, like a response from a chatbot and please Note when question as give me list then provide answer as like list. content={all_content} and question={userMessage}"
        )
        formating = prompt.format(all_content=all_content, userMessage=text)
        output_name = llm(formating)

        return output_name

    except Exception as e:
        print(e)


def labeling_intent(df,report_path, df_name = "train"):
    try:
        if df_name == "train":
            di_main = {}
            all_intent_list = list(df["label"].value_counts().keys())
            for num, var in enumerate(all_intent_list):
                di_main[var] = num
                df["label"] = df["label"].replace(var, num)
            app.config["SPACY_DICT"] = di_main
            json_str = json.dumps(di_main)

            with open(report_path+"my_file.json", "w") as f:
                f.write(json_str)
            return df
        else:
            with open(report_path+"my_file.json", "r") as f:
                json_str = f.read()

            # Convert JSON string to dictionary
            di_main = json.loads(json_str)
            if di_main:
                all_intent_list = list(df["label"].value_counts().keys())
                for var in all_intent_list:
                    df["label"] = df["label"].replace(var, di_main[var])

            return df, di_main

    except Exception as e:
        print(e)
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/", methods=["GET", "POST"])
def home():
    try:
        return render_template("index.html")
    except Exception as e:
        print(e)

@app.route("/process", methods=["GET", "POST"])
def process():
    try:
        if request.method == "POST":
            count = request.form["model_name"]

            data_path = os.path.abspath("data")+"/model_"+str(count)+"/"
            report_path = os.path.abspath("reports")+"/model_"+str(count)+"/"
            output_path = os.path.abspath("output")+"/model_"+str(count)+"/"
            path = app.config["UPLOAD_FOLDER"] + "model_" + str(count) + "/"
            if not os.path.exists(path):
                os.mkdir(path)
            if not os.path.exists(data_path):
                os.mkdir(data_path)
            if not os.path.exists(report_path):
                os.mkdir(report_path)
            if not os.path.exists(output_path):
                os.mkdir(output_path)

            find_dict = {"cp_name": count}
            data = find_all_specific_user(coll_name="training_data", di=find_dict)
            all_data = [[d["intent"], d["phrase"]] for d in data]

            dfd = pd.DataFrame(all_data, columns=["label", "text"])
            dfd1 = pd.DataFrame(all_data, columns=["label", "text"])

            df = labeling_intent(dfd, report_path=report_path)
            df.to_csv(path+"train.csv", index=False)

            df1, di_main = labeling_intent(dfd1, df_name="val", report_path=report_path)
            df1.to_csv(path+"eval.csv", index=False)

            res_main = model_process(path, data_path, output_path, report_path)

            count = count+1
            app.config["COUNT"] = count

            return redirect(url_for('home', _external=True, _scheme=secure_type))
        else:
            return render_template("index.html")

    except Exception as e:
        return redirect(url_for('home', _external=True, _scheme=secure_type))

@app.route("/prediction_text", methods=["GET", "POST"])
def prediction_text():
    try:
        if request.method == "POST":
            text = request.form.get('text')
            count = request.form.get("cp_name")

            report_path = os.path.abspath("reports") + "/model_" + str(count) + "/"
            output_path = os.path.abspath("output") + "/model_" + str(count) + "/"

            with open(report_path+"my_file.json", "r") as f:
                json_str = f.read()

            di_main = json.loads(json_str)

            prediction_label, int_label, confidence, other_confidence = prediction_model(text, di_main, output_path)
            print(f"intent_name:{prediction_label}, confidence: {confidence}")

            if confidence>=80 and prediction_label!="intent_affirm" and prediction_label!="intent_deny":
                response = get_data_response(cp_name=count, intent_name=prediction_label)
            else:
                print("chatgpt answer")
                con_di = {"cp_name": count}
                content_data = find_all_specific_user(coll_name="content_data", di=con_di)
                con_data = [con["content"] for con in content_data]
                response = get_res_by_chatgpt(text=text, all_content=con_data[0])

            question_di = {"cp_name": count, "question": text, "answer": response.strip(), "inserted_on": datetime.datetime.now()}
            register_data(coll_name="conversational_data", new_dict=question_di)

            return render_template("index_prediction.html", response=response)
        else:
            return render_template("index_prediction.html")

    except Exception as e:
        return redirect(url_for('home', _external=True, _scheme=secure_type))



if __name__ == "__main__":
    # db.create_all()
    app.run(
        host="127.0.0.1",
        port="5000",
        debug=True)
